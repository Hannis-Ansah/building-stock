# Building Stock EU28

In this repository is published the Building stock for the EU28.

## Description
The present task provides data with following characteristics:

<table>
  <tr>
    <td> </td>
    <td><b>Spatial resolution</b></td>
    <td><b>Temporal resolution</b></td>
  </tr>
  <tr>
    <td><b>Building characteristics</b></td>
    <td>NUTS0</td>
    <td>-</td>
  </tr>
  <tr>
    <td><b>Nearly zero-energy buildings</b></td>
    <td>NUTS0</td>
    <td>-</td>
  </tr>
  <tr>
    <td><b>Building surface volume ratio</b></td>
    <td>Raster @ 100 X 100 m</td>
    <td>-</td>
  </tr>
</table>

**Table 1**. Characteristics of data provided within Task 2.1 Building stock analysis.

The data collected in the building stock analysis are used as starting point to calculate the useful energy demand (UED) for space heating (SH), space cooling (SC), and domestic hot water (DHW) for each EU28 MS down to its local level (Task 2.2), and to derive scenarios for the future development of the UED. The Hotmaps toolbox generates raster maps with characteristic building stock indicators (UED, gross floor area, etc.) with a resolution of 100 x 100 m covering the entire EU28 building stock. The map is based on aggregated values at NUTS0, using, among others, the population (EUROSTAT: CENSUS 2011 [8]) land-use data (CORINE land cover, 2006 [1]), the European Settlement Map layer [2], the data from the Global Human Settlement project [9], and data from the OpenStreetMap database as proxy. 

Furthermore, within this task, we derive the UED layer (“Heat density map”) using the raster map of building stock characteristics (gross floor area, building volume, share by construction period, building surface-to-volume ratio), the Digital Elevation Model (EU DEM) and the climatic data retrieved in Task 2.3. Based on this analysis, basic statistics on the need per UED category could be extracted for the EU at regional/local level.

## Methodology

Data have been collected per country, and organized within the residential and service sectors, addressing specific types of buildings and time periods. 
The residential sector has been subdivided based on the following building typologies: 
-	Single family houses (SFHs);
-	Multifamily houses (MFHs); 
-	Apartment blocks (ABs – high-rise buildings that contain several dwellings and have more than four storeys [10]). 
The service sector includes the following categories:
-	Offices: composed of private and public offices; this section includes also office blocks;
-	Trade: individual shops, department stores, shopping centres, grocery shops, car sales and garages, bakeries, hairdresser, service stations, laundries, congress and fair buildings, and other wholesale and retail infrastructures;
-	Education: primary, secondary and high schools. Furthermore, universities, infrastructure for professional training activities, school dormitories, and research centres/laboratories are part of this sector;
-	Health: private and public hospitals, nursing homes, medical care centres;
-	Hotels and restaurants: hotels, hostels, cafés, pubs, restaurants, canteens, and catering in business;
-	Other non-residential buildings: warehouses, transportation and garage buildings, military barracks, agricultural  buildings (farms, greenhouses), and sport facilities (e.g. sport halls, swimming pools, and gyms) [11].
In order to present a complete picture of the MSs’ building stock and to describe time-related specifications, the following construction periods have been defined: 
-	Before 1945: buildings constructed before 1945 are generally classified as historic buildings. The historic building stock is highly inhomogeneous, making it difficult to apply a standardized assessment. Nevertheless, certain characteristics may still be generalized, such as the use of massive construction methodologies for residential buildings;
-	1945-1969: buildings erected after World War II and before 1969 are generally characterized by nearly missing insulation and inefficient energy systems, caused by the choice of cheap construction materials and by short construction times. These results in higher specific UED; 
-	1970-1979: buildings built between 1970 and 1979 present the first insulation applications (as a consequence to the world energy crises of the 1970´s); 
-	1980-1989 and 1990-1999: buildings constructed during these two periods reflect the introduction of the first national thermal efficiency ordinances (around 1990);
-	2000-2010: buildings considered to be influenced by the impact of the EU Energy Performance of Buildings Directive (2002/91/EC and following recasts);
-	After 2010: recently constructed buildings are analysed to understand the impact of the economic crisis on Europe´s construction branch. The present analysis contains data updated until the year 2016.

With regard to the building typologies and construction periods previously described, the following features have been analysed:

- Constructed, heated, and cooled floor areas [Mm²]
- Number of dwellings/units, and of buildings [Mil.]
- Owner occupied, private rented, social housing dwellings/units [Mil.]
- Occupied, vacant, and secondary dwellings units – and others [A] [Mil.]
- Thermal transmittance – U-values – walls, windows, roof, and floor [W/m² K]
- Construction materials and methodologies
  - Walls:
    - Construction material – brick, concrete, wood, others[B] [%]
    - Construction methodology – solid wall, cavity wall, honeycomb - bricks/hollow
      - blocks wall, others[C] [%]       – insulation or not [%]  	
  - Windows:
    - Construction material      	– wood, synthetic/pvc, aluminium [%]
    - Construction methodology – single glazing, double glazing, triple glazing [%]
    - low-emittance or not [%]
  - Roof:
    - Construction material  	– wood, concrete, concrete + bricks [%]
    - Construction technology – tilted, flat – insulation or not [%]
  - Floor:
    - Construction material      	– wood, concrete, concrete + bricks, and others[D] [%]
    - Construction methodology – concrete slab, wooden floor, others[E] [%]
    - insulation or not [%]
- Technologies for SH, SC, and DHW
  - Technologies used for SH:
    - Individual, central, or district heating [level of presence]
    - Boiler (condensing or not), combined, stove, electric heating,
    - Solar Collectors, Heat pumps [level of presence]
    - Fossil fuels (solid, liquid, gas), electricity, biomass [level of presence]
  - Technologies used for SC:
    - SC or not [level of presence]
  - Technologies used for DHW preparation:
    - Individual, central, or district heating [level of presence]
    - Boiler (condensing or not), combined, stove, electric heating,
    - Solar Collectors, Heat pumps [level of presence]
    - Fossil fuels (solid, liquid, gas), electricity, biomass [level of presence]
- UED
  - SH [kWh/m² y], [TWh/y]
  - SC [kWh/m² y], [TWh/y]
  - DHW [kWh/m² y], [TWh/y]
- FEC
  - SH [kWh/m² y], [TWh/y]
  - SC [kWh/m² y], [TWh/y]
  - DHW [kWh/m² y], [TWh/y]

* * *
[A] Abusive homes, neither registered, occupied nor vacant or secondary dwellings

[B] Construction materials less frequently used, such as stone or stone/brick and stone/concrete mixed structures

[C] Prefabricated panels and lightweight exterior walls

[D]  Mainly stone floors

[E] Less diffused construction technologies for floors (e.g. vaulted or coffered ceilings)
* * *

Concerning the collected information, it is important to distinguish between useful energy demand (UED) and final energy consumption (FEC). The UED represents the net energy required to cover SH, SC, and DHW needs. On the other hand, the FEC is the empirically measured energy input into the supply system, which is required to satisfy the abovementioned demand. The two quantities thus differ by disparate conversion factors, which take into account the efficiency of each supply technology as well as the distribution losses, but may also differ due to user behaviour. For example, if SH and DHW is provided by a boiler, the FEC is higher than the UED, since the efficiency of this technology is < 1 (0.8-0.9 for currently installed technologies in Europe). On the contrary, the FEC for space cooling is lower than the UED in case of electrically driven technologies (e.g. heat pumps) that have an energy efficiency ratio greater than one (EER > 1 - around 2-3 for currently installed technologies within the EU). It has to be stressed that, while it is correct to compare UED for SH, SC and DHW purposes, FEC in form of electricity (in heat pumps and air-conditioners) can be compared to fuel consumption (e.g. gas in a gas boiler) only by performing an adequate conversion into primary energy. Indeed, the two energy carriers have a different content of grey energy; primary energy (usually expressed in kWh or toe) accounts for the use of resources (fossils and non) providing a basis for a correct comparison among different energy carriers [12].

Data quality, completeness, accuracy, and reliability are key aspects in the process of generating the default datasets of the Hotmaps project. Hence, the following features have been taken into consideration in this process:

- Data inventory;
- Data reliability;
- Data definition and comparability

### Data inventory

One of the major challenges in developing an inventory of UED data for SH, SC, and DHW in different sectors is to provide an almost complete list of existing information. In general, the advantage of using data coming from EU information providers and EU projects is that these are available for large territories (e.g. BPIE [13]). However, the data provided are rarely fully complete. Therefore, national statistics have been used as data sources with the aim to increase data coverage.

The data collection process implied not only extrapolating and assembling data from data tools available online (e.g. TABULA [14]), but also researching data source-by-source from single scientific literature sources as journal papers (e.g. [15]), conference proceedings (e.g. [16]) and project deliverables (e.g. [17]). Only through such an in-depth approach, the already mentioned lacks of data per energy type (SH, SC and DHW) and nation, could be filled.

One important aspect of the data inventory is to ensure the understandability and correct interpretability of information. Together with the data, standardized structured information is provided, including the specification of author/s, titles, time reference, and if available the universal resource locator (URL).

### Data reliability

All sources taken into consideration have been analysed to assess the reliability of the gathered data. In particular, the methodology applied to generate data of the utilized fonts has been taken to a closer look. Furthermore, the gaps in information have been completed by in-depth investigations on scientific literature.

### Data definition and comparability

The data have been collected per country, with reference to the most recent year; the majority of data refer to the year 2016. Despite the majority of the data providers utilize standardized data formats and units, this does not necessarily mean that data are fully comparable. Adjusting differences and inconsistencies among different data characteristics (e.g. time references) to improve data comparability is one of the most important aspects in the process of data elaboration.

Apart from the use within Hotmaps and other existing tools, the developed database is expected to improve data quality for users in the energy sector, and to provide data useful to monitor the progress towards the achievement of the goals defined in EU energy related Directives.

In the following paragraphs, the main sources and the methodology of data elaboration are described for all the main features in the database. The data regarding *Covered area* have been retrieved for each construction sector, building type and period, from Invert/EE-Lab database [18]. The total values for the residential and service sectors have been obtained by summing the data of all the building typologies for each time period. With regard to the heated and cooled floor area, data refer to several sources; among all, the most used are [19] - [22]. With regard to the *Tenure/ownership status and distribution* and *Occupancy* fields of the database, the data for the residential sector have been obtained from the EU Building Stock Observatory [23]; while for the service sector, several sources have been used for each MS.

The section Construction features contains the U-values of the main building elements (i.e. walls, windows, roof, and floor). The data have been obtained for each building typology from TABULA web-tool [24] for the residential sector, and from the EU building database [25] and the results of the project iNSPiRe [20] for the service sector. The total values of thermal transmittance for each sector have been calculated by weighting the U-values of the single subsector with the respective constructed floor area.

The main sources for the sections *Construction materials and methodologies*, and *Technologies* for *SH, SC and DHW* for the residential sector is the TABULA web-tool [14]. The descriptions of the construction features have been collected for each building typology (SFHs, MFHs, and ABs) and construction period. Data has been organized in sub-sections for walls, windows, roof and floor. The percentages presented in the database resulted from weighting the data for the total floor area of each building typology.

Similarly to the construction features, also the data concerning SH, SC and DHW have been mainly collected using the TABULA Web-tool. However, the web site indicates for each building typology and construction period only the most widespread technology. For this reason, the database section *Technologies for SH, SC and DHW*, does not contain the data in percentage, but indicates only the diffusion of each technology and fuel. The data has been calculated for the total residential sector, weighted on the total floor area of each building typology, and has been grouped based on the percentage of diffusion as follows:

- \>75%: most widespread technology/fuel;
- 25% to 75%: widespread technology/fuel;
- < 25%: less widespread technology/fuel.

With regard to the service sector, the TABULA Web-tool does not contain any data. Furthermore, the scientific sources detailing typical construction features and technologies for SH, SC and DHW are scarce. Hence, an expert questioning has been carried out. A questionnaire containing all features already included in the database for the residential sector has been sent to two experts per country. The collected data has been analysed and the results have been clustered in geographical areas. The utilized questionnaire is attached in the annex (see Section 4), while the filled questionnaires are not enclosed and the names of the interviewees are not listed for privacy issues.

The clusters, based on the geographical proximity of the countries, are the following:

- Northern Europe: Denmark, Finland, Sweden, Estonia, Latvia, Lithuania;
- Central Europe: Austria, Belgium, Germany, Netherland, Luxembourg, France, United Kingdom, and Ireland;
- Eastern Europe: Poland, Czech Republic, Hungary, Slovenia, Slovakia, Croatia, Bulgaria, Romania;
- Southern Europe: Spain, Italy, Greece, Cyprus, Malta, and Portugal.

The results have been counterchecked with the few sources available on the topic [26] - [28].

The main source for the fields *Useful energy demand* for space heating, cooling, and DHW is Invert/EE-Lab database [29]. Based on these values, the *Final energy consumption* has been calculated by multiplying the useful energy demand by the ratio values obtained in Task 2.2. The *Total useful energy demand* have been obtained as follows:

- Space heating + domestic hot water [TWh/year]: ((Mm² heated floor area * SH demand) + (Mm² total floor area * DHW demand)) / 1000
- Space cooling [TWh/year]: (Mm² cooled floor area * SC demand) / 1000

Finally, the *Total final energy consumption* has been calculated with the following equations:

- Space heating + domestic hot water [TWh/year]: ((Mm² heated floor area * SH consumption) + (Mm² total floor area * DHW consumption)) / 1000
- Space cooling [TWh/year]: (Mm² cooled floor area * SC consumption) / 1000

With regard to all the sections of the database, the totals *Residential sector_Total* and *Service sector_Total* (units: Mm², Mil., TWh/y) have been obtained by summing up the values for the respective subsectors.

Since it was not possible to fill all the cells of the database, estimations have been performed for the missing data. Please see Section 2.1.2.1 for further information on these estimations.

## Repository structure

Files:
```
datapackage.json            Datapackage JSON file with the main meta-data
data/
    building_stock.csv      CSV data with the normalized data set
    DHW_spaceHC.csv         CSV data with the normalized data set
    building_stock.xlsx     GRASS GIS raster color rules
scripts/
    xlsx2csv.py             Python code to transform the xlsx file into a csv
    requirements.txt        Python requirements file
```

## How to convert the raw xlsx file to the CSV file

Install mandatory dependencies using:
```bash
$ pip install -r scripts/requirements.txt
```

Or f you are using Python through Conda:
```bash
$ conda create -n new environment --file scripts/requirements.txt
```

Once all the dependencies are installed, you can execute:
```bash
$ python scripts/xlsx2csv.py
```

This script will convert the file `data/building_stock.xlsx` into `data/building_stock.csv`.


## How to cite

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW) Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 www.hotmaps-project.eu


## Authors

Simon Pezzutto, Silvia Croce, Stefano Zambotti


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

## References
* [1] European Environmental Agency, “Corine Land Cover 2006,” 2016. [Online]. Available: [http://ec.europa.eu/eurostat/web/population-and-housing-census/census-data/2011-census](http://ec.europa.eu/eurostat/web/population-and-housing-census/census-data/2011-census).
* [2] Joint Research Center, “European Settlement Map.” copernicus, 2017.
* [8] European Statistical System, “Census Hub,” 2011. [Online]. Available: [http://ec.europa.eu/eurostat/web/population-and-housing-census/census-data/2011-census](http://ec.europa.eu/eurostat/web/population-and-housing-census/census-data/2011-census).
* [9] Pesaresi, Martino; Ehrilch, Daniele; Florczyk, Aneta J.; Freire, Sergio; Julea, Andreea; Kemper, Thomas; Soille, Pierre; Syrris, Vasileios, “GHS built-up grid, derived from Landsat, multitemporal (1975, 1990, 2000, 2014). [Dataset].” European Commission, Joint Research Centre (JRC), 2015.
* [10] Ministry of the Interior and Kingdom Relations, “Housing Statistics in the European Union 2010,” OTB Research Institute for the Built Environment, Delft University of Technology, The Hague, 2010.
* [11] Buildings Performance Institute Europe, Europe’s Buildings under Microscope. A country-by-country review of the energy performance of buildings. 2011.
* [12] S. Pezzutto, “Analysis of the space heating and cooling market in Europe,” Doctoral Thesis, University of Natural Resources and Life Sciences, Vienna, 2014.
* [13] Building Performance Institute Europe, “Data Hub for the Energy Performance of Buildings,” 2015. [Online]. Available: [https://www.buildingsdata.eu/](https://www.buildingsdata.eu/).
* [14] TABULA, “TABULA WebTool,” 2016. [Online]. Available: [http://webtool.building-typology.eu/#bm](http://webtool.building-typology.eu/#bm).
* [15] S. Pezzutto, S. Toleikyte, and M. De Felice, “Assessment of the space heating and cooling market in the EU28: a comparison between EU15 and EU13 member states.,” Int. J. Contemp. Energy, vol. 2, pp. 35–48, 2015.
* [16] H. Mahlknecht, S. Avesani, M. Benedikter, and G. Felderer, “Refurbishment and monitoring of an historical building. A case Study.,” presented at the 47°AICARR Int. Conference, Tivoli, Rome, 2009.
* [17] S. Birchall, I. Wallis, D. Churcher, S. Pezzutto, R. Fedrizzi, and E. Causse, “iNSPiRe. D2.1a - Survey on the energy needs and architectural features of the EU building stock,” 2014.
* [18] Vienna University of Technology, “Invert/EE-Lab,” 2015. [Online]. Available: [http://www.invert.at/](http://www.invert.at/).
* [19] J. Adnot, “Energy Efficiency and Certification of Central Air Conditioners (EECCAC). Final Report,” Apr. 2003.
* [20] S. Birchall, I. Wallis, D. Churcher, S. Pezzutto, R. Fedrizzi, and E. Causse, “iNSPiRe. D2.1a - Survey on the energy needs and architectural features of the EU building stock,” 2014.
* [21] Werner, Sven, “The European Heat Market. Final Report,” Euroheat & Power, 2016.
* [22] ODYSSEE-MURE, “ODYSSEE database on energy efficiency data & indicators,” 2017. [Online]. Available: [http://www.indicators.odyssee-mure.eu/energy-efficiency-database.html](http://www.indicators.odyssee-mure.eu/energy-efficiency-database.html).
* [23] EU Building Stock Observatory, “EU Building Stock Observatory,” 2015. [Online]. Available: [https://ec.europa.eu/energy/en/eu-buildings-database](https://ec.europa.eu/energy/en/eu-buildings-database).
* [24] TABULA, “TABULA WebTool,” 2016. [Online]. Available: [http://webtool.building-typology.eu/#bm](http://webtool.building-typology.eu/#bm).
* [25] EU Building Stock Observatory, “EU Buildings Database,” 2015. [Online]. Available: [https://ec.europa.eu/energy/en/eu-buildings-database](https://ec.europa.eu/energy/en/eu-buildings-database).
* [26] L. Itard and F. Meijer, “Towards a sustainable Northern European housing stock. Figures, facts and future.,” TU Delft, 22, 2008.
* [27] R. Ortlepp, K. Gruhler, G. Schiller, and C. Deilmann, “The other „half of the city“. Analysis of non-residential building stock and its materials.,” in SASBE 2015 Proceedings, Pretoria, South Africa, 2015.
* [28] TABULA Project Team, “Typology approaches for non-residential buildings in five European Countries – Existing information, concepts and outlook.,” Deliverable D.9, 2012.
* [29] Vienna University of Technology, “Invert/EE-Lab,” 2015. [Online]. Available: [http://www.invert.at/](http://www.invert.at/).

## License

Copyright © 2016-2018: Simon Pezzutto <simon.pezzutto@eurac.edu>
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html